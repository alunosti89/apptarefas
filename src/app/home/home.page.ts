import { Component } from '@angular/core';
import { AlertController, ToastController, ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
tarefas: any[]=[]  // matriz de tarefas (nome, feito)
  constructor(private alertCtrl: AlertController, 
    private toastCtrl: ToastController,
    private actionSheetCtrl: ActionSheetController) {
      let tarefaJson = localStorage.getItem('tarefaDb');
      if(tarefaJson != null)
      {
        this.tarefas = JSON.parse(tarefaJson);
      }
    }

async addTarefa(){
  const alerta = await this.alertCtrl.create({
    header:'O que você precisa fazer?',
    inputs:[
      {name: 'txtnome', type:'text', placeholder:'digite aqui...'},
    // {name: 'txtcpf', type:'text', placeholder:'cpf'},
     //{name: 'txtfone', type:'text', placeholder:'telefone'},
    ],
    buttons:[
      {text: 'Cancelar',role:'cancel', cssClass:'light',
      handler: ()=>{
        // caso o usuário clique em cancelar
        console.log('Você cancelou...');
      }},
      {
        text:'Ok', handler:(form)=>{
        // debugger;
          this.add(form.txtnome);
        }
      }
    ]
  });
  alerta.present();
}
 async add(nova: any) {
    if(nova.trim().length < 1){
      const toast = await this.toastCtrl.create({
        message:"Informe o que precisa fazer",
        duration:2000,
        position:'middle',
        color:'warning'
      });
      toast.present();
      return;
    }
    let tarefa = {nome: nova, feito:false};
    this.tarefas.push(tarefa);
    //armazenar no celular
    this.atualizaLocalStorage();
    console.log(this.tarefas);
  }

excluir(tarefa:any){
  this.tarefas = this.tarefas.filter(a => tarefa != a);
  this.atualizaLocalStorage();
}
async abrirOpcoes(tarefa: any){
const actionSheet = await this.actionSheetCtrl.create({
  header:'Escolha uma ação',
  buttons:[
    {text:tarefa.feito?'Desmarcar':'Marcar',
    icon:tarefa.feito?'radio-button-off':'checkmark-circle',
    handler:()=>{
      tarefa.feito=!tarefa.feito;
      this.atualizaLocalStorage();
    }
    },{
      text:'Cancelar',
      icon:'close',
      role:'cancel',
      handler:()=>{
        console.log('ok, cancelado');
      }
    }
  ]
});
actionSheet.present();
}//
atualizaLocalStorage(){
  localStorage.setItem('tarefaDb', JSON.stringify(this.tarefas));
}

}
